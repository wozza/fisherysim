class LakesController < ApplicationController

  before_filter :require_login, :only =>[:create, :update, :destroy, :edit, :new]
  before_action :get_lake, :only => [:show, :edit, :update, :destroy, :check_user_owns]
  before_action :check_user_owns, only: [:edit, :update, :destroy]

  def get_lake
    @lake = Lake.find(params[:id])
  end

  def check_user_owns
    if @lake.user != current_user
      flash[:notice] = "Unable to edit, you don't own this lake!"
      redirect_to request.referer
    end
  end

  def new
    @lake = Lake.new
  end

  def index

  end

  def show
      fish_in_lake = @lake.marine_animals.pluck(:species)
      @fishes_info = fish_in_lake.each_with_object(Hash.new(0)) { |e, h| h[e] += 1}
  end

  def edit
  end

  def create
      @lake = Lake.new(lake_params)
      @lake.user_id = current_user.id

      if @lake.save
        redirect_to @lake
      else
        render 'new'
      end
  end

  def update
      if @lake.update(lake_params)
        redirect_to @lake
      else
        render 'edit'
      end
  end

  def destroy
      @lake.destroy
      redirect_to lakes_path
  end

  private
    def lake_params
        params.require(:lake).permit(:name,:size,:fish_density);
    end

end
