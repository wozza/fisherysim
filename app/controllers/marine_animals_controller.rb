class MarineAnimalsController < ApplicationController

  before_filter :require_login, :only =>[:create, :update, :destroy, :edit, :new]
  before_action :get_marine_animal, only: [:edit, :show, :update, :destroy, :check_user_owns]
  before_action :check_user_owns, only: [:edit, :update, :destroy]

  def get_marine_animal
    @marine_animal = MarineAnimal.find(params[:id])
  end

  def check_user_owns
    if @marine_animal.user != current_user
      flash[:notice] = "You cannot edit this marine animal as you don't own it"
      redirect_to request.referer
    end
  end

  def index
    if params.has_key?(:species)
      @species = params[:species]
      @marine_animals_to_list = MarineAnimal.where(:species => params[:species])
    else
      @marine_animals_to_list = MarineAnimal.all
    end

    #if there is a lake_id, then there is no need to display the lake column in the iew

   if params.has_key?(:lake_id)
      @lake = Lake.find(params[:lake_id]).name
   end
  end

  def new
      @marine_animal = MarineAnimal.new
  end

  def edit
  end

  def show
  end

  def update
     if @marine_animal.update(marine_animals_params)
       redirect_to @marine_animal
     else
       render 'edit'
     end
  end

  def create
    if params.has_key?(:lake_id)
      @lake = Lake.find(params[:lake_id])
      @marine_animal = MarineAnimal.new(marine_animals_params)
      @marine_animal.user_id = current_user.id

      if @marine_animal.save
        @lake.marine_animals.push(@marine_animal)
        redirect_to lake_path(@lake)
      else
        flash[:errors] = @marine_animal.errors.full_messages
        redirect_to lake_path(@lake)
      end
    else
      @marine_animal = MarineAnimal.new(marine_animals_params)
      @marine_animal.user_id = current_user.id
      if @marine_animal.save
        redirect_to @marine_animal
      else
        render 'new'
      end
    end
  end

  def destroy
      @marine_animal.destroy
      redirect_to marine_animals_path
  end

  private
    def marine_animals_params
      params.require(:marine_animal).permit(:age,:weight,:species,:lake_id)
    end

end
