class Lake < ActiveRecord::Base
  validates :name, presence: true, length: {minimum: 2, maximum: 20}
  validates :size, presence:true, numericality: {only_integer: true}
  validates :fish_density, presence: true, inclusion: {in:['low','medium', 'high']}

  has_many :marine_animals
  belongs_to :user
end
