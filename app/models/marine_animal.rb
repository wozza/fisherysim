class MarineAnimal < ActiveRecord::Base
  validates :species, presence: true, length: {maximum: 40}
  validates :weight, presence: true, numericality: {greater_than: 0}
  validates :age, presence: true, numericality: {only_integer: true, greater_than: 0}

  belongs_to :lake
  belongs_to :user
end
