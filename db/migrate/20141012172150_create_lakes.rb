class CreateLakes < ActiveRecord::Migration
  def change
    create_table :lakes do |t|
      t.string :name
      t.integer :size
      t.string :fish_density

      t.references :user, index: true, null: false

      t.timestamps
    end
  end
end
