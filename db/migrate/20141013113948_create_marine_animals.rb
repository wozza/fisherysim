class CreateMarineAnimals < ActiveRecord::Migration
  def change
    create_table :marine_animals do |t|
      t.string :species
      t.decimal :weight
      t.integer :age

      t.references :lake, index: true
      t.references :user, index: true

      t.timestamps
    end
  end
end
