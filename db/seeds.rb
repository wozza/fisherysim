# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user = User.create! :email => 'john@gmail.com', :password => 'topsecret', :password_confirmation => 'topsecret'

Lake.create(name: "Bonds lake", size:3, fish_density: "low", user_id: 1)

MarineAnimal.create(species: "Cod", weight: "33.4", age: "3", lake_id: 1, user_id: 1)
MarineAnimal.create(species: "Cod", weight: "2.4", age: "1", lake_id: 1, user_id: 1)
MarineAnimal.create(species: "Bass", weight: "333.4", age: "4", lake_id: 1, user_id: 1)


